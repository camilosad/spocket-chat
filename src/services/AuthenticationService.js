import { browserHistory } from 'react-router';
import ActionCableService from './ActionCableService';

export default class AuthenticationServie {

  login(token, user) {
    this.setToken(token);
    this.setCurrentUser(user);
    browserHistory.replace('/chat');
  }

  logout() {
    const ac = new ActionCableService();
    ac.unsubscribe();
    localStorage.removeItem('spocket_token');
    localStorage.removeItem('spocket_current_user');
    browserHistory.replace('/login');
  }

  setToken(token) {
    localStorage.setItem('spocket_token', token);
  }

  setCurrentUser(user) {
    localStorage.setItem('spocket_current_user', user);
  }

  getToken() {
    return localStorage.getItem('spocket_token');
  }

  getCurrentUser() {
    return localStorage.getItem('spocket_current_user');
  }

  loggedIn() {
    return !!this.getToken();
  }
}
