import ActionCable from 'actioncable';
import { WS_URL } from './../config/constants';

export default class ActionCableService {

  createConsumer() {
    window.App = {};
    window.App.cable = ActionCable.createConsumer(WS_URL);
  }

  subscribe(userEmail, received) {
    this.unsubscribe();
    this.createConsumer();
    window.App.ChatSubscription = window.App.cable.subscriptions.create({
      channel: "ChatChannel", user: userEmail
    }, {
      received: received
    });
  }

  unsubscribe() {
    if (!this.hasChatSubscription()) return false;

    this.getChatSubscription().unsubscribe();
    this.deleteChatSubscription();
  }

  hasChatSubscription() {
    return !!window.App && !!window.App.ChatSubscription;
  }

  getChatSubscription() {
    return window.App.ChatSubscription;
  }

  deleteChatSubscription() {
    delete window.App.ChatSubscription;
  }

}