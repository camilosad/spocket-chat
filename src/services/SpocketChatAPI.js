import { API_URL } from './../config/constants';

const LOGIN_ENDPOINT = `${API_URL}/v1/user_token`;
const GET_CONTACTS_ENDPOINT = `${API_URL}/v1/users`;
const GET_MESSAGES_ENDPOINT = `${API_URL}/v1/messages`;

const login = (user) => {
  const body = {
    auth: {
      email: user.email,
      password: user.password
    }
  };

  return fetch(LOGIN_ENDPOINT, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })
  .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return response.json().then(err => { throw err });
    }
  })
  .catch((error) => { throw error });
};


const getContacts = (token) => {
  return fetch(GET_CONTACTS_ENDPOINT, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `JWT ${token}`
    }
  })
 .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return response.json().then(err => { throw err });
    }
  })
  .catch((error) => {throw error});
};

const getMessages = (token) => {
  return fetch(GET_MESSAGES_ENDPOINT, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `JWT ${token}`
    }
  })
 .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return response.json().then(err => { throw err });
    }
  })
  .catch((error) => {throw error});
};


const postMessage = (token, message) => {
  const body = { message: message };

  return fetch(GET_MESSAGES_ENDPOINT, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `JWT ${token}`
    },
    body: JSON.stringify(body)
  })
  .then(response => {
    if (response.ok) {
      return response.json();
    } else {
      return response.json().then(err => { throw err });
    }
  })
  .catch((error) => { throw error });
};


export default {
  login,
  getContacts,
  getMessages,
  postMessage
}