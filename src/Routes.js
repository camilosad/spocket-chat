import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Router, Route, IndexRedirect, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { Provider } from 'react-redux';
import App from './components/containers/App';
import AuthenticationContainer from './modules/authentication/components/AuthenticationContainer';
import ChatContainer from './modules/chat/components/ChatContainer';
import AuthService from './services/AuthenticationService';
import ActionCableService from './services/ActionCableService';
import { receiveMessage } from './modules/chat/actions/index';

class Routes extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { store } = this.props;
    const historyWithRedux = syncHistoryWithStore(browserHistory, store);
    const auth = new AuthService();
    const ac = new ActionCableService();

    const authenticate = (nextState, replace) => {
      if (auth.loggedIn()) {
        ac.subscribe(auth.getCurrentUser(), (data) => {
          store.dispatch(receiveMessage(data));
        });
      } else {
        replace({ pathname: '/login' });
      }
    };


    return (
      <Provider store={store} >
        <Router history={historyWithRedux}>
          <Route path="/" component={App}>
            <IndexRedirect to="/chat" />
            <Route path="/chat" component={ChatContainer} onEnter={authenticate} />
          </Route>
          <Route path="/login" component={AuthenticationContainer} />
        </Router>
      </Provider>
    );
  }
}

Routes.propTypes = {
  store: PropTypes.object.isRequired
};

export default Routes;
