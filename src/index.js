import ReactDOM from 'react-dom';
import React from 'react';
import Routes from './Routes';
import configureStore from './store/configureStore';
import '@blueprintjs/core/dist/blueprint.css';
import './index.css';

const store = configureStore({});

ReactDOM.render(<Routes store={store} />, document.getElementById('root'));