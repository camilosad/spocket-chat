import * as types from './../actions/types';
import * as authentication from './reducer';

export const initialState = {
  token: null,
  isLoading: false,
  error: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.LOGIN_REQUEST:
      return authentication.loginRequest(state);
    case types.LOGIN_SUCCESS:
      return authentication.loginSuccess(state, action);
    case types.LOGIN_FAILURE:
      return authentication.loginFailure(state, action);
    case types.LOGOUT:
      return authentication.logout(state);
    default:
      return state;
  }
}
