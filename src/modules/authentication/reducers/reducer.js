export function loginRequest(state) {
  return { ...state, isLoading: true };
}

export function loginFailure(state, action) {
  return { ...state, isLoading: false, error: action.payload };
}

export function loginSuccess(state, action) {
  return { ...state, isLoading: false, token: action.payload };
}

export function logout(state) {
  return { ...state, isLoading: false, token: null };
}
