import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as authenticationActions from './../actions/index';
import LoginForm from './../../../components/authentication/LoginForm';
import './AuthenticationContainer.css';

class AuthenticationContainer extends Component {

  render() {
    const { login } = this.props.authenticationActions;
    const { isLoading } = this.props.authenticationState;

    return (
      <div className="content-body login-container">
        <div className="row center-xs">
          <div className="col-xs-12 col-sm-8 col-md-6 col-lg-4">
            <p className="auth-form-title"> Spocket Chat </p>
            <hr />
            <LoginForm login={login} isLoading={isLoading}/>
          </div>
        </div>
      </div>
    );
  }
}

AuthenticationContainer.propTypes = {
  authenticationActions: PropTypes.object.isRequired,
  authenticationState: PropTypes.object.isRequired
};

export default connect(
  state => ({
    authenticationState: state.authentication
  }),
  dispatch => ({
    authenticationActions: bindActionCreators(authenticationActions, dispatch),
  })
)(AuthenticationContainer);
