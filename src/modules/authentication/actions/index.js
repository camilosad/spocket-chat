import * as types from './types';
import { receiveMessage } from './../../chat/actions/index';
import SpocketChatAPI from './../../../services/SpocketChatAPI';
import AuthService from './../../../services/AuthenticationService';
import ActionCableService from './../../../services/ActionCableService';
import { errorNotification } from './../../../components/shared/notifications';

const auth = new AuthService();
const ac = new ActionCableService();

export function action(type, payload) {
  if (payload) {
    return { type, payload };
  }

  return { type };
}

export function login(user) {
  return (dispatch) => {
    dispatch(action(types.LOGIN_REQUEST));

    SpocketChatAPI
      .login(user)
      .then((response) => {
        const { jwt } = response;
        dispatch(action(types.LOGIN_SUCCESS, jwt));
        auth.login(jwt, user.email);
        ac.subscribe(user.email, (data) => {
          dispatch(receiveMessage(data));
        });
      })
      .catch((error) => {
        dispatch(action(types.LOGIN_FAILURE, error));
        errorNotification('Invalid credentials');
      });
  };
}

export function logout() {
  return (dispatch) => {
    dispatch(action(types.LOGOUT));
    auth.logout();
    ac.unsubscribe();
  };
}
