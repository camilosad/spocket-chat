export function fetchContactsRequest(state) {
  return { ...state, isLoading: true, error: null, contacts: null };
}

export function fetchContactsFailure(state, action) {
  return { ...state, isLoading: false, error: action.payload };
}

export function fetchContactsSuccess(state, action) {
  return { ...state, isLoading: false, contacts: action.payload, error: null };
}


export function fetchMessagesRequest(state) {
  return { ...state, isLoading: true, error: null, messages: null };
}

export function fetchMessagesFailure(state, action) {
  return { ...state, isLoading: false, error: action.payload };
}

export function fetchMessagesSuccess(state, action) {
  return { ...state, isLoading: false, messages: action.payload, error: null };
}


export function sendMessageRequest(state) {
  return { ...state, error: null };
}

export function sendMessageFailure(state, action) {
  return { ...state, error: action.payload };
}

export function sendMessageSuccess(state, action) {
  return { ...state, messages: [...state.messages, action.payload], error: null };
}

export function receiveMessageSuccess(state, action) {
  return { ...state, messages: [...state.messages, action.payload], error: null };
}
