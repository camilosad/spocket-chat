import * as types from './../actions/types';
import * as chat from './reducer';

export const initialState = {
  contacts: null,
  messages: null,
  isLoading: false,
  error: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case types.FETCH_CONTACTS_REQUEST:
      return chat.fetchContactsRequest(state);
    case types.FETCH_CONTACTS_SUCCESS:
      return chat.fetchContactsSuccess(state, action);
    case types.FETCH_CONTACTS_FAILURE:
      return chat.fetchContactsFailure(state, action);
    case types.FETCH_MESSAGES_REQUEST:
      return chat.fetchMessagesRequest(state);
    case types.FETCH_MESSAGES_SUCCESS:
      return chat.fetchMessagesSuccess(state, action);
    case types.FETCH_MESSAGES_FAILURE:
      return chat.fetchMessagesFailure(state, action);
    case types.SEND_MESSAGE_REQUEST:
      return chat.sendMessageRequest(state);
    case types.SEND_MESSAGE_SUCCESS:
      return chat.sendMessageSuccess(state, action);
    case types.SEND_MESSAGE_FAILURE:
      return chat.sendMessageFailure(state, action);
    case types.RECEIVE_MESSAGE_SUCCESS:
      return chat.receiveMessageSuccess(state, action);
    default:
      return state;
  }
}
