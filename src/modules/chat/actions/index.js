import * as types from './types';
import SpocketChatAPI from './../../../services/SpocketChatAPI';
import {
  errorNotification,
  successNotification
} from './../../../components/shared/notifications';

export function action(type, payload) {
  if (payload) {
    return { type, payload };
  }

  return { type };
}

export function fetchContacts(token) {
  return (dispatch) => {
    dispatch(action(types.FETCH_CONTACTS_REQUEST));

    SpocketChatAPI
      .getContacts(token)
      .then((response) => {
        dispatch(action(types.FETCH_CONTACTS_SUCCESS, response));
      })
      .catch((error) => {
        dispatch(action(types.FETCH_CONTACTS_FAILURE, error));
        errorNotification('Could not fetch contacts');
      });
  };
}

export function fetchMessages(token) {
  return (dispatch) => {
    dispatch(action(types.FETCH_MESSAGES_REQUEST));

    SpocketChatAPI
      .getMessages(token)
      .then((response) => {
        dispatch(action(types.FETCH_MESSAGES_SUCCESS, response));
      })
      .catch((error) => {
        dispatch(action(types.FETCH_MESSAGES_FAILURE, error));
        errorNotification('Could not fetch messages');
      });
  };
}

export function sendMessage(token, message) {
  return (dispatch) => {
    dispatch(action(types.SEND_MESSAGE_REQUEST));

    SpocketChatAPI
      .postMessage(token, message)
      .then((response) => {
        dispatch(action(types.SEND_MESSAGE_SUCCESS, response));
      })
      .catch((error) => {
        dispatch(action(types.SEND_MESSAGE_FAILURE, error));
        errorNotification('Could not send message');
      });
  };
}

export function receiveMessage(message) {
  return (dispatch) => {
    dispatch(action(types.RECEIVE_MESSAGE_SUCCESS, message));
    successNotification(`New message from ${message.sender_email}`);
  };
}