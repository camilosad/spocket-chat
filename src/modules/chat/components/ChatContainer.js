import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import AuthService from './../../../services/AuthenticationService';
import * as chatActions from './../actions/index';
import ContactList from './../../../components/contacts/ContactList';
import Chat from './../../../components/chat/Chat';
import Loading from './../../../components/shared/Loading';

class ChatContainer extends Component {

  constructor(props) {
    super(props);

    this.state = { activeContact: '' };
    this.renderChat = this.renderChat.bind(this);
    this.renderContactList = this.renderContactList.bind(this);
    this.changeContact = this.changeContact.bind(this);
    this.findContact = this.findContact.bind(this);
    this.chatMessages = this.chatMessages.bind(this);
    this.renderLoading = this.renderLoading.bind(this);
    this.renderContent = this.renderContent.bind(this);
    this.isMessageFromOrToUser = this.isMessageFromOrToUser.bind(this);
    this.getCurrentUser = this.getCurrentUser.bind(this);
  }

  componentDidMount() {
    const { chatActions, authenticationState } = this.props;
    const auth = new AuthService();
    const token = authenticationState.token || auth.getToken();

    chatActions.fetchContacts(token);
    chatActions.fetchMessages(token);
  }


  findContact(contactEmail) {
    const { contacts } = this.props.chatState;
    return contacts.find( contact => contact.email === contactEmail );
  }

  isMessageFromOrToUser(message, contactEmail) {
    return message.sender_email === contactEmail ||
            message.receiver_email === contactEmail;
  }

  chatMessages(contactEmail) {
    const { messages } = this.props.chatState;
    return messages.filter( message => this.isMessageFromOrToUser(message, contactEmail) );
  }

  changeContact(contactEmail) {
    this.setState({ activeContact: contactEmail });
  }

  renderContactList() {
    const { activeContact } = this.state;
    const { contacts } = this.props.chatState;

    return (
      <ContactList
        contacts={contacts}
        activeContact={activeContact}
        changeContact={this.changeContact}
      />
    );
  }

  noChatMessage() {
    return (
      <div className="pt-text-muted text-centered pt-ui-text-large">
        Select a contact to chat...
      </div>
    );
  }

  renderChat() {
    const { activeContact } = this.state;
    const { sendMessage } = this.props.chatActions;
    const contact = this.findContact(activeContact);
    const history = this.chatMessages(activeContact);

    return <Chat contact={contact} history={history} sendMessage={sendMessage} />;
  }

  renderChatSection() {
    const { activeContact } = this.state;

    return !!activeContact ? this.renderChat() : this.noChatMessage();
  }

  renderLoading() {
    return <div className="row center-xs"><Loading /></div>;
  }

  renderContent() {
    return (
      <div>
        <div className="row">
          <div className="col-md-4">
            {this.renderContactList()}
          </div>
          <div className="col-md-8">
            {this.renderChatSection()}
          </div>
        </div>
      </div>
    );
  }

  getCurrentUser() {
    const auth = new AuthService();
    return auth.getCurrentUser();
  }

  render() {
    const { isLoading, contacts, messages } = this.props.chatState;
    const isDataReady = !isLoading && !!contacts && !!messages && !!this.getCurrentUser();

    return isDataReady ? this.renderContent() : this.renderLoading();
  }
}

ChatContainer.propTypes = {
  chatActions: PropTypes.object.isRequired,
  chatState: PropTypes.object.isRequired,
  authenticationState: PropTypes.object.isRequired
};

export default connect(
  state => ({
    chatState: state.chat,
    authenticationState: state.authentication
  }),
  dispatch => ({
    chatActions: bindActionCreators(chatActions, dispatch),
  })
)(ChatContainer);
