export const API_URL = process.env.NODE_ENV === 'production' ?
                      'https://spocket-chat-api.herokuapp.com' :
                      'http://localhost:5000';

export const WS_URL =  process.env.NODE_ENV === 'production' ?
                       'wss://spocket-chat-api.herokuapp.com/cable' :
                       'ws://localhost:5000/cable';