import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Button } from '@blueprintjs/core';
import AuthService from './../../services/AuthenticationService';

class ChatInput extends Component {

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.clearInput = this.clearInput.bind(this);
  }

  clearInput() {
    this.msg.value = "";
  }

  handleSubmit(e) {
    e.preventDefault();
    if (!this.msg.value) return false;

    const { contact, sendMessage } = this.props;
    const auth = new AuthService();
    const token = auth.getToken();
    const newMessage = { content: this.msg.value, receiver_id: contact.id };

    sendMessage(token, newMessage);
    this.clearInput();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="row">
          <div className="col-md-11">
            <input
              id="msg"
              ref={(msg) => { this.msg = msg } }
              className="pt-input pt-fill"
              type="text"
              placeholder="Type a message"
              dir="auto"
            />
          </div>
          <div className="col-md-1">
            <Button
              iconName="play"
              type="submit"
              className="pt-intent-primary pt-minimal"
            />
          </div>
        </div>
      </form>
    );
  }

}

ChatInput.propTypes = {
  contact: PropTypes.object.isRequired,
  sendMessage: PropTypes.func.isRequired
};

export default ChatInput;