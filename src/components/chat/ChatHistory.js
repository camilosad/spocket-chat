import React from 'react';
import { PropTypes } from 'prop-types';
import Message from './Message';
import './ChatHistory.css';

const ChatHistory = ({ messages }) => {

  const content = messages.map((message, index) => {
    return (
      <div key={index}>
        <Message message={message} />
      </div>
    );
  });

  const noMessagesText = () => {
    return <div className="pt-text-muted"> No previous messages </div>
  };

  const messagesHistory = () => {
    return <div className='history-container'>{ content }</div>;
  };

  return messages.length ? messagesHistory() : noMessagesText();

}

ChatHistory.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default ChatHistory;
