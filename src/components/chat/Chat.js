import React from 'react';
import { PropTypes } from 'prop-types';
import ChatHeader from './ChatHeader';
import ChatHistory from './ChatHistory';
import ChatInput from './ChatInput';

const Chat = ({ contact, history, sendMessage }) => {
    return (
      <div className="pt-card pt-elevation-1">
        <ChatHeader contact={contact} />
        <hr />
        <ChatHistory messages={history} />
        <hr />
        <ChatInput contact={contact} sendMessage={sendMessage} />
      </div>
    );
}

Chat.propTypes = {
  sendMessage: PropTypes.func.isRequired,
  contact: PropTypes.object.isRequired,
  history: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Chat;
