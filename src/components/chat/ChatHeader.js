import React from 'react';
import { PropTypes } from 'prop-types';

const ChatHeader = ({ contact }) => {

  return (
    <div>
      <h5>{contact.name}</h5>
      <p>{contact.email}</p>
    </div>
  );

}

ChatHeader.propTypes = {
  contact: PropTypes.object.isRequired
};

export default ChatHeader;