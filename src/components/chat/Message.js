import React from 'react';
import { PropTypes } from 'prop-types';
import AuthService from './../../services/AuthenticationService';
import './Message.css'

const Message = ({ message }) => {
  const auth = new AuthService();
  const currentUser = auth.getCurrentUser();
  let messageClass = 'message ';
  messageClass += message.sender_email === currentUser ?
                        'sent-message' : 'received-message';

  return (
    <div className={messageClass}>
      <div>{ message.content }</div>
      <span className="message-timestamps pt-text-muted">{ message.sent_at } </span>
    </div>
  );

}

Message.propTypes = {
  message: PropTypes.object.isRequired
};

export default Message;