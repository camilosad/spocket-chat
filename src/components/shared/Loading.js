import React from 'react';
import { Spinner } from '@blueprintjs/core';

const Loading = ({ modifier = null }) => {
  return <Spinner className={modifier}/>;
};

export default Loading;