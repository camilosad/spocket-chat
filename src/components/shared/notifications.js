import { Position, Toaster, Intent } from '@blueprintjs/core';

const baseNotification = Toaster.create({
  position: Position.BOTTOM_RIGHT,
  canEscapeKeyClear: true,
  className: 'mb-s'
});

export function successNotification(text) {
  baseNotification.show({
    message: text,
    intent: Intent.SUCCESS,
    iconName: 'tick'
  });
}

export function errorNotification(text) {
  baseNotification.show({
    message: text,
    intent: Intent.DANGER,
    iconName: 'error'
  });
}