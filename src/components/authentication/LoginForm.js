import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Button } from '@blueprintjs/core';
import { errorNotification } from './../shared/notifications';

class LoginForm extends Component {

  constructor(props) {
    super(props);

    this.validateEmail = this.validateEmail.bind(this);
    this.checkEmptyField = this.checkEmptyField.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

 validateEmail(email) {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  checkEmptyField(field) {
    const re = /([^\s])/;
    return re.test(field);
  }

  validateForm() {
    const isEmailEmpty = !this.checkEmptyField(this.email.value);
    const isPasswordEmpty = !this.checkEmptyField(this.password.value);
    const isEmailValid = this.validateEmail(this.email.value);

    if (isEmailEmpty || isPasswordEmpty) {
      errorNotification('Please fill in both fields');
      return false;
    }

    if (!isEmailValid) {
      errorNotification('Please enter a valid email');
      return false;
    }

    return true;
  }

  handleSubmit(e) {
    e.preventDefault();
    const isFormValid = this.validateForm();

    if (isFormValid) {
      const user = { email: this.email.value, password: this.password.value };
      this.props.login(user);
    }
  }


  render() {
    const { isLoading } = this.props;

    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <div className="pt-control-group pt-vertical" >
            <div className="pt-input-group pt-large">
              <span className="pt-icon pt-icon-person" />
              <input
                id="email"
                className="pt-input"
                type="text"
                placeholder="Email"
                ref={(email) => { this.email = email; }}
              />
            </div>
            <div className="pt-input-group pt-large">
              <span className="pt-icon pt-icon-lock" />
              <input
                id="password"
                className="pt-input"
                type="password"
                placeholder="password"
                ref={(password) => { this.password = password; }}
              />
            </div>
              <Button
                type='submit'
                className='pt-large pt-intent-primary'
                text='Login'
                disabled={isLoading}
              />
          </div>
        </form>
      </div>
    );
  }
}

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default LoginForm;
