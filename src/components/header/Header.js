import React from 'react';
import AuthService from './../../services/AuthenticationService';

const auth = new AuthService();

const Header = () => {
  return (
    <div className="">
      <nav className="pt-navbar pt-fixed-top pt-dark">
        <div className="container">
          <div className="pt-navbar-group pt-align-left">
            <div className="pt-navbar-heading">
              <a className="pt-button pt-minimal" role="button" tabIndex="0">
                <strong>Spocket Chat</strong>
              </a>
            </div>
          </div>
          <div className="pt-navbar-group pt-align-right">
            <span>{auth.getCurrentUser()}</span>
            <span className="pt-navbar-divider"></span>
            <button type="button" className="pt-button pt-minimal" onClick={auth.logout}>
              Logout
              <span className="pt-icon-standard pt-align-right pt-icon-log-out" />
            </button>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Header;
