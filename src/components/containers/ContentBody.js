import React from 'react';
import { PropTypes } from 'prop-types';

const ContentBody = ({ children }) => {
  return (
    <div className="content-body">
      {children}
    </div>
  );
};

ContentBody.propTypes = {
  children: PropTypes.object.isRequired
};

export default ContentBody;
