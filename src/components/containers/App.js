import React from 'react';
import { PropTypes } from 'prop-types';

import ContentWrapper from './ContentWrapper';
import ContentBody from './ContentBody';
import Header from './../header/Header';

const App = ({ route, children }) => {
  return (
    <ContentWrapper>
      <Header />
      <ContentBody>
        {children}
      </ContentBody>
    </ContentWrapper>
  );
};

App.propTypes = {
  route: PropTypes.object.isRequired,
  children: PropTypes.element.isRequired
};

export default App;
