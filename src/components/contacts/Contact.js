import React from 'react';
import { PropTypes } from 'prop-types';
import './Contact.css';

const Contact = ({ contact, active }) => {
  let contactClass = "pt-card pt-elevation-1 contactCard";
  if (active) { contactClass += ' activeContact' }

  return (
    <div className={contactClass}>
      <h5>{contact.name}</h5>
      <p>{contact.email}</p>
    </div>
  );

}

Contact.propTypes = {
  contact: PropTypes.object.isRequired,
  active: PropTypes.bool.isRequired
};

export default Contact;
