import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import _ from 'lodash';
import Contact from './Contact';

class ContactList extends Component {

  constructor(props) {
    super(props);

    this.renderContact = this.renderContact.bind(this);
    this.renderContactList = this.renderContactList.bind(this);
    this.renderNoDataCallout = this.renderNoDataCallout.bind(this);
  }

  renderNoDataCallout() {
    return (
      <div className="pt-callout pt-icon-info-sign">
        <h5>You have no contacts</h5>
      </div>
    );
  }

  renderContact(contact) {
    const { changeContact, activeContact } = this.props;
    const active = contact.email === activeContact;

    return (
      <div key={contact.email} onClick={() => changeContact(contact.email)}>
        <Contact contact={contact} active={active} />
        <hr />
      </div>
    );
  }

  renderContactList() {
    const { contacts } = this.props;
    const sortedContacts = _.sortBy(contacts, ['name', 'email']);

    return sortedContacts.map((contact) => {
      return this.renderContact(contact);
    });
  }

  render() {
    const { contacts } = this.props;

    if (!contacts.length) {
      return this.renderNoDataCallout();
    }

    return (
      <div> {this.renderContactList()} </div>
    );
  }

}

ContactList.propTypes = {
  contacts: PropTypes.arrayOf(PropTypes.object).isRequired,
  changeContact: PropTypes.func.isRequired,
  activeContact: PropTypes.string.isRequired
};

export default ContactList;
