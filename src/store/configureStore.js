import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import reducers from './reducers';

function configureStore(initialState) {
  const logger = createLogger();
  const enhancer = compose(
    applyMiddleware(
      thunk,
      logger
    )
  );
  return createStore(reducers, initialState, enhancer);
}

export default configureStore;
