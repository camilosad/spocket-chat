import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import authentication from './../modules/authentication/reducers/index';
import chat from './../modules/chat/reducers/index';

export default combineReducers({
  authentication,
  chat,
  routing: routerReducer
});
